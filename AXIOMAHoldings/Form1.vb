﻿Public Class Form1
    Dim getdata As DataAccess = New DataAccess
    Dim dtDates As DataTable = New DataTable
    Dim grp As String = ""
    Dim filedate As String = ""
    Dim startdate As Date = DateAdd(DateInterval.Day, -1, Today())
    Dim enddate As Date = DateAdd(DateInterval.Day, -1, Today())
    Dim frequency As String = "daily"
    Dim outpath As String = "M:\Temporary Data Holding\AXIOMA\Manual\"

#Region "Connection Strings"
    Dim connPM As String = ""
    Dim connDM As String = ""
    Dim connQA As String = ""
    Dim connAPX As String = ""
    Dim connMXY As String = ""
    Dim connMCM As String = ""
    Public Sub GetConnectionStrings()
        Dim conpath As String = "\\coder\Information Systems\Jesica\"
        Dim objTR As System.IO.TextReader
        objTR = System.IO.File.OpenText(conpath & "SQLConnections.ini")
        While objTR.Peek <> -1
            Dim splitout As String() = Split(objTR.ReadLine(), vbTab)
            If splitout(0) = "PortMGMT" Then
                connPM = splitout(1)
            End If
            If splitout(0) = "DataMGMT" Then
                connDM = splitout(1)
            End If
            If splitout(0) = "QACustom" Then
                connQA = splitout(1)
            End If
            If splitout(0) = "APX" Then
                connAPX = splitout(1)
            End If
            If splitout(0) = "MOXY" Then
                connMXY = splitout(1)
            End If
            If splitout(0) = "MCM_UP" Then
                connMCM = splitout(1)
            End If
        End While
        objTR.Close()
    End Sub
#End Region

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GetConnectionStrings()
        Me.dtpStart.Value = startdate
        Me.dtpEnd.Value = enddate
        Me.rdoDaily.Checked = True
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        'delete data in holdings table
        getdata.DeleteAppraisalData(connDM)

        startdate = dtpStart.Value
        enddate = dtpEnd.Value

        dtDates = GetDates()
        Dim datect As Integer = dtDates.Rows.Count - 1
        enddate = dtDates.Rows(datect).Item("Date")
        If startdate = enddate Then
            filedate = startdate.ToString("MMddyyyy")
        ElseIf frequency = "custom" Then
            filedate = "customdates"
        Else
            filedate = startdate.ToString("MMddyyyy") & "-" & enddate.ToString("MMddyyyy")
        End If
        Dim dtAppr As DataTable = New DataTable

        If txtGrpAcct.Text <> "" Then
            grp = txtGrpAcct.Text
            Dim rundate As Date

            For x As Integer = 0 To dtDates.Rows.Count - 1
                rundate = dtDates.Rows(x).Item(0)
                lblDisplay.Text = "Running appraisal query: " & rundate.ToString("MM/dd/yyyy")
                System.Windows.Forms.Application.DoEvents()
                Me.Refresh()
                dtAppr = getdata.GetAppraisalData(connAPX, grp, rundate)
                getdata.CopyToDataMGMT(connDM, dtAppr)
            Next
        Else
            MsgBox("Please enter group/account name", MsgBoxStyle.Critical, "Missing Data")
        End If

        Dim dtAxData As DataTable = New DataTable
        dtAxData = getdata.GetAxiomaData(connDM)
        WriteDataToText(dtAxData)

        lblDisplay.Text = "Complete"
        System.Windows.Forms.Application.DoEvents()
        Me.Refresh()
        Process.Start("explorer.exe", outpath)
    End Sub

    Private Sub WriteDataToText(ByVal dtappr As DataTable)
        Dim objTW As System.IO.TextWriter
        objTW = System.IO.File.CreateText(outpath & grp & "_" & filedate & ".txt")
        For x As Integer = 0 To dtappr.Rows.Count - 1
            objTW.WriteLine(dtappr.Rows(x).Item(0) & vbTab & dtappr.Rows(x).Item(1) & vbTab & dtappr.Rows(x).Item(2) & vbTab & dtappr.Rows(x).Item(3))
        Next
        objTW.Close()
    End Sub

    Private Function GetDates() As DataTable
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Date")

        Select Case frequency
            Case "daily", "monthly"
                dt = getdata.GetDates(connPM, startdate, enddate, frequency)
            Case "custom"
                Dim splDates As String() = Split(txtDates.Text, vbCrLf)

                For x As Integer = 0 To splDates.GetUpperBound(0)
                    Dim newrow As DataRow = dt.NewRow
                    newrow("Date") = splDates(x)
                    dt.Rows.Add(newrow)
                Next
        End Select
        Return dt
    End Function

    Private Sub rdoDaily_CheckedChanged(sender As Object, e As EventArgs) Handles rdoDaily.CheckedChanged
        CheckDates()
    End Sub

    Private Sub rdoMonthly_CheckedChanged(sender As Object, e As EventArgs) Handles rdoMonthly.CheckedChanged
        CheckDates()
    End Sub

    Private Sub rdoCustom_CheckedChanged(sender As Object, e As EventArgs) Handles rdoCustom.CheckedChanged
        CheckDates()
    End Sub

    Private Sub CheckDates()
        If rdoDaily.Checked = True Then
            frequency = "daily"
            Me.dtpStart.Enabled = True
            Me.dtpEnd.Enabled = True
            Me.txtDates.Enabled = False
            Me.lblCustom.Visible = False
        ElseIf rdoMonthly.Checked = True Then
            frequency = "monthly"
            Me.dtpStart.Enabled = True
            Me.dtpEnd.Enabled = True
            Me.txtDates.Enabled = False
            Me.lblCustom.Visible = False
        Else
            frequency = "custom"
            Me.dtpStart.Enabled = False
            Me.dtpEnd.Enabled = False
            Me.txtDates.Enabled = True
            Me.lblCustom.Visible = True
        End If
    End Sub
End Class
