﻿Imports System.Data.SqlClient

Public Class DataAccess

    Public Function GetDates(ByVal connPM As String, ByVal startdate As Date, ByVal enddate As Date, ByVal frequency As String) As DataTable
        Dim conn As OleDb.OleDbConnection = New OleDb.OleDbConnection(connPM & "Provider=SQLOLEDB;")

        Dim sql As String = ""
        If frequency = "daily" Then
            sql = "Select HoldDate as Date from HoldingsDates " & _
                                        "where HoldDate between '" & startdate & "' and '" & enddate & "'"
        Else
            sql = "Select [Date] from MonthEndDates " & _
                                        "where [Date] between '" & startdate & "' and '" & enddate & "'"
        End If

        Dim cmd As OleDb.OleDbCommand = New OleDb.OleDbCommand(sql, conn)
        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow
        Dim dt As New DataTable

        da.Fill(ds, "dtDates")

        dt.Columns.Add("Date", GetType(System.DateTime))

        For Each drDSRow In ds.Tables("dtDates").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Date") = drDSRow("Date")
            dt.Rows.Add(drNewRow)
        Next
        Return dt
    End Function

    Public Sub DeleteAppraisalData(ByVal connDM As String)
        Dim sql As String = "DELETE FROM dbo.Axioma_APXAppraisal"
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connDM)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)
        cmd.CommandType = CommandType.Text

        Try
            conn.Open()
            cmd.ExecuteNonQuery()
            conn.Close()
        Catch ex As Exception
            Debug.WriteLine(ex.ToString, "Error deleting data Axioma_APXAppraisal")
        End Try
    End Sub

    Public Function GetAppraisalData(ByVal connAPX As String, ByVal grp As String, ByVal apprdate As Date) As DataTable
        Dim conn As OleDb.OleDbConnection = New OleDb.OleDbConnection(connAPX & "Provider=SQLOLEDB;")
        Dim cmd As OleDb.OleDbCommand = New OleDb.OleDbCommand("spAPX_AxiomaAppraisal_REG", conn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 30000

        With cmd.Parameters
            .AddWithValue("@DATE", apprdate)
            .AddWithValue("@GRP", grp)
        End With


        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow
        Dim dt As New DataTable

        da.Fill(ds, "dtDates")

        dt.Columns.Add("ReportDate", GetType(System.DateTime))
        dt.Columns.Add("PortfolioBaseCode", GetType(System.String))
        dt.Columns.Add("SecTypeCode", GetType(System.String))
        dt.Columns.Add("ProprietarySymbol", GetType(System.String))
        dt.Columns.Add("SecuritySymbol", GetType(System.String))
        dt.Columns.Add("BB_Underlying", GetType(System.String))
        dt.Columns.Add("VendorUniqueID", GetType(System.String))
        dt.Columns.Add("PercentAssets", GetType(System.Decimal))

        For Each drDSRow In ds.Tables("dtDates").Rows()
            drNewRow = dt.NewRow()
            drNewRow("ReportDate") = drDSRow("ReportDate")
            drNewRow("PortfolioBaseCode") = drDSRow("PortfolioBaseCode")
            drNewRow("SecTypeCode") = drDSRow("SecTypeCode")
            drNewRow("ProprietarySymbol") = drDSRow("ProprietarySymbol")
            drNewRow("SecuritySymbol") = drDSRow("SecuritySymbol")
            drNewRow("BB_Underlying") = drDSRow("BB_Underlying")
            drNewRow("VendorUniqueID") = drDSRow("VendorUniqueID")
            drNewRow("PercentAssets") = drDSRow("PercentAssets")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function

    Public Sub CopyToDataMGMT(ByVal connDM As String, ByVal dt As DataTable)
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connDM)
        conn.Open()

        Using s As SqlBulkCopy = New SqlBulkCopy(conn)
            s.DestinationTableName = "dbo.Axioma_APXAppraisal"
            s.WriteToServer(dt)
            s.Close()
        End Using

        conn.Close()
    End Sub

    Public Function GetAxiomaData(ByVal connDM As String) As DataTable
        Dim conn As OleDb.OleDbConnection = New OleDb.OleDbConnection(connDM & "Provider=SQLOLEDB;")
        Dim cmd As OleDb.OleDbCommand = New OleDb.OleDbCommand("spAxioma_GetHoldingsManual", conn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandTimeout = 30000

        Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter(cmd)
        Dim ds As New DataSet
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow
        Dim dt As New DataTable

        da.Fill(ds, "dtAxData")

        dt.Columns.Add("Name", GetType(System.String))
        dt.Columns.Add("HoldDate", GetType(System.String))
        dt.Columns.Add("AxiomaID", GetType(System.String))
        dt.Columns.Add("Weight", GetType(System.Decimal))

        For Each drDSRow In ds.Tables("dtAxData").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Name") = drDSRow("Name")
            drNewRow("HoldDate") = drDSRow("HoldDate")
            drNewRow("AxiomaID") = drDSRow("AxiomaID")
            drNewRow("Weight") = drDSRow("Weight")
            dt.Rows.Add(drNewRow)
        Next

        Return dt
    End Function
End Class
