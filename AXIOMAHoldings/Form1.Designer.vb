﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpEnd = New System.Windows.Forms.DateTimePicker()
        Me.dtpStart = New System.Windows.Forms.DateTimePicker()
        Me.gbxFrequency = New System.Windows.Forms.GroupBox()
        Me.rdoCustom = New System.Windows.Forms.RadioButton()
        Me.lblCustom = New System.Windows.Forms.Label()
        Me.txtDates = New System.Windows.Forms.TextBox()
        Me.rdoMonthly = New System.Windows.Forms.RadioButton()
        Me.rdoDaily = New System.Windows.Forms.RadioButton()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtGrpAcct = New System.Windows.Forms.TextBox()
        Me.lblDisplay = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.gbxFrequency.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.dtpEnd)
        Me.GroupBox2.Controls.Add(Me.dtpStart)
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(34, 268)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(228, 125)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Run Dates"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(14, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 23)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "End Date"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(14, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 23)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Start Date"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEnd
        '
        Me.dtpEnd.Location = New System.Drawing.Point(14, 92)
        Me.dtpEnd.Name = "dtpEnd"
        Me.dtpEnd.Size = New System.Drawing.Size(200, 20)
        Me.dtpEnd.TabIndex = 3
        '
        'dtpStart
        '
        Me.dtpStart.Location = New System.Drawing.Point(14, 43)
        Me.dtpStart.Name = "dtpStart"
        Me.dtpStart.Size = New System.Drawing.Size(200, 20)
        Me.dtpStart.TabIndex = 2
        '
        'gbxFrequency
        '
        Me.gbxFrequency.BackColor = System.Drawing.Color.Transparent
        Me.gbxFrequency.Controls.Add(Me.rdoCustom)
        Me.gbxFrequency.Controls.Add(Me.lblCustom)
        Me.gbxFrequency.Controls.Add(Me.txtDates)
        Me.gbxFrequency.Controls.Add(Me.rdoMonthly)
        Me.gbxFrequency.Controls.Add(Me.rdoDaily)
        Me.gbxFrequency.Location = New System.Drawing.Point(34, 54)
        Me.gbxFrequency.Name = "gbxFrequency"
        Me.gbxFrequency.Size = New System.Drawing.Size(227, 208)
        Me.gbxFrequency.TabIndex = 2
        Me.gbxFrequency.TabStop = False
        Me.gbxFrequency.Text = "Frequency"
        '
        'rdoCustom
        '
        Me.rdoCustom.AutoSize = True
        Me.rdoCustom.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.rdoCustom.Location = New System.Drawing.Point(15, 74)
        Me.rdoCustom.Name = "rdoCustom"
        Me.rdoCustom.Size = New System.Drawing.Size(60, 17)
        Me.rdoCustom.TabIndex = 4
        Me.rdoCustom.TabStop = True
        Me.rdoCustom.Text = "Custom"
        Me.rdoCustom.UseVisualStyleBackColor = True
        '
        'lblCustom
        '
        Me.lblCustom.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblCustom.Location = New System.Drawing.Point(6, 153)
        Me.lblCustom.Name = "lblCustom"
        Me.lblCustom.Size = New System.Drawing.Size(76, 49)
        Me.lblCustom.TabIndex = 3
        Me.lblCustom.Text = "Add dates (MM/dd/yyyy), one per line"
        '
        'txtDates
        '
        Me.txtDates.Location = New System.Drawing.Point(90, 17)
        Me.txtDates.Multiline = True
        Me.txtDates.Name = "txtDates"
        Me.txtDates.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDates.Size = New System.Drawing.Size(118, 183)
        Me.txtDates.TabIndex = 2
        '
        'rdoMonthly
        '
        Me.rdoMonthly.AutoSize = True
        Me.rdoMonthly.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.rdoMonthly.Location = New System.Drawing.Point(15, 51)
        Me.rdoMonthly.Name = "rdoMonthly"
        Me.rdoMonthly.Size = New System.Drawing.Size(62, 17)
        Me.rdoMonthly.TabIndex = 1
        Me.rdoMonthly.TabStop = True
        Me.rdoMonthly.Text = "Monthly"
        Me.rdoMonthly.UseVisualStyleBackColor = True
        '
        'rdoDaily
        '
        Me.rdoDaily.AutoSize = True
        Me.rdoDaily.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.rdoDaily.Location = New System.Drawing.Point(15, 28)
        Me.rdoDaily.Name = "rdoDaily"
        Me.rdoDaily.Size = New System.Drawing.Size(48, 17)
        Me.rdoDaily.TabIndex = 1
        Me.rdoDaily.TabStop = True
        Me.rdoDaily.Text = "Daily"
        Me.rdoDaily.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(57, 399)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(178, 23)
        Me.btnRun.TabIndex = 4
        Me.btnRun.Text = "Run Reports"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label3.Location = New System.Drawing.Point(35, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Group/Account"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtGrpAcct
        '
        Me.txtGrpAcct.Location = New System.Drawing.Point(138, 19)
        Me.txtGrpAcct.Name = "txtGrpAcct"
        Me.txtGrpAcct.Size = New System.Drawing.Size(110, 20)
        Me.txtGrpAcct.TabIndex = 0
        Me.txtGrpAcct.Text = "catuk9"
        '
        'lblDisplay
        '
        Me.lblDisplay.BackColor = System.Drawing.Color.Transparent
        Me.lblDisplay.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblDisplay.Location = New System.Drawing.Point(31, 425)
        Me.lblDisplay.Name = "lblDisplay"
        Me.lblDisplay.Size = New System.Drawing.Size(228, 23)
        Me.lblDisplay.TabIndex = 6
        Me.lblDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(296, 461)
        Me.Controls.Add(Me.lblDisplay)
        Me.Controls.Add(Me.txtGrpAcct)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.gbxFrequency)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "Form1"
        Me.Text = "Axioma Holdings"
        Me.GroupBox2.ResumeLayout(False)
        Me.gbxFrequency.ResumeLayout(False)
        Me.gbxFrequency.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbxFrequency As System.Windows.Forms.GroupBox
    Friend WithEvents rdoMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents rdoDaily As System.Windows.Forms.RadioButton
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents txtDates As System.Windows.Forms.TextBox
    Friend WithEvents rdoCustom As System.Windows.Forms.RadioButton
    Friend WithEvents lblCustom As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtGrpAcct As System.Windows.Forms.TextBox
    Friend WithEvents lblDisplay As System.Windows.Forms.Label

End Class
