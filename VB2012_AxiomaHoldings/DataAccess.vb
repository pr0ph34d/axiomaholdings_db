﻿Public Class DataAccess

    Dim connAPX As String = "Data Source=APXSQL15;Initial Catalog=APXFirm;Integrated Security=SSPI;"
    Dim connPM As String = "Data Source=SQLPROD;Initial Catalog=PortfolioMGMT;Integrated Security=SSPI;"

    Public Function ClientExists(ByVal acct As String) As Boolean
        Dim sql As String
        If acct.Contains("@") Then
            sql = "select * from dbo.AoObject o " & _
                            "where o.classID = 88 " & _
                            "and name = '" & acct.Replace("@", "") & "'"
        Else
            sql = "select * from dbo.AoObject o " & _
                            "where o.classID = 76 " & _
                            "and name = '" & acct & "'"
        End If
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connAPX)

        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(sql, conn)

        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        Dim ds As DataSet = New DataSet
        da.Fill(ds)
        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function GetDates(ByVal sdate As Date, ByVal edate As Date, ByVal period As String) As DataTable
        Dim sql As String = ""
        If period = "daily" Then
            sql = "select Date from HoldingsDates where holddate between '" & sdate & "' and '" & edate & "'"
        Else
            sql = "select HoldDate as Date from HoldingsDates where holddate between '" & sdate & "' and '" & edate & "'"
        End If
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPM)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Sql, conn)

        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        Dim ds As New DataSet
        Dim drDSRow As DataRow
        Dim drNewRow As DataRow
        Dim dt As New DataTable

        da.Fill(ds, "dtDates")

        dt.Columns.Add("Date", GetType(System.DateTime))

        For Each drDSRow In ds.Tables("dtDates").Rows()
            drNewRow = dt.NewRow()
            drNewRow("Date") = drDSRow("Date")
            dt.Rows.Add(drNewRow)
        Next
        Return dt
    End Function

    Public Function GetHoldings(ByVal acct As String, ByVal rdate As Date) As DataSet
        Dim conn As SqlClient.SqlConnection = New SqlClient.SqlConnection(connPM)
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("spAPX_AxiomaAppraisal", conn)
        cmd.CommandType = CommandType.StoredProcedure

        With cmd.Parameters
            .AddWithValue("@DATE", rdate)
            .AddWithValue("@grp", acct)
        End With

        Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)
        Dim ds As New DataSet
        Dim dt As New DataTable

        conn.Open()
        da.Fill(ds, "dtHoldings")
        conn.Close()

        Return ds
    End Function

End Class
