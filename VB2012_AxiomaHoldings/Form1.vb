﻿Public Class Form1

    Dim reppath As String = "M:\Temporary Data Holding\AXIOMA\ManualReports\"
    Dim outpath As String = "M:\Temporary Data Holding\AXIOMA\ManualOutput\"
    Dim acct As String = ""
    Dim fname As String = ""
    Dim period As String = "monthly"
    Dim sdate As DateTime
    Dim edate As DateTime
    Dim getdata As DataAccess = New DataAccess

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        acct = Me.txtAcct.Text
        sdate = Me.dtpStart.Value
        edate = Me.dtpEnd.Value
        If rdoDay.Checked = True Then
            period = "daily"
        Else
            period = "monthly"
        End If

        Dim dt As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        dt = getdata.GetDates(sdate, edate, period)
        Dim rdate As Date

        Dim x As Integer = 0
        For x = 0 To dt.Rows.Count - 1
            rdate = dt.Rows(x).Item("Date")

            ds = getdata.GetHoldings(acct, rdate)
        Next

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.txtAcct.Focus()
        GetLastMonthDates()
        Me.dtpStart.Value = sdate
        Me.dtpEnd.Value = edate
    End Sub

    Private Sub txtAcct_Leave(sender As Object, e As EventArgs) Handles txtAcct.Leave
        If getdata.ClientExists(Me.txtAcct.Text) Then
            acct = Me.txtAcct.Text
        Else
            MsgBox("'" & Me.txtAcct.Text & "' is not a valid APX group or account, please re-enter.", MsgBoxStyle.Information, "Invalid Entry")
        End If
    End Sub

    Private Sub txtAcct_TextChanged(sender As Object, e As EventArgs) Handles txtAcct.TextChanged
        GetLastMonthDates()
        Me.txtFilename.Text = Me.txtAcct.Text.Replace("@", "") & "_" & sdate.ToString("MMddyyyy") & "-" & edate.ToString("MMddyyyy") & ".txt"
    End Sub

    Private Sub GetLastMonthDates()
        Dim today As DateTime = DateTime.Today()
        Dim lastmonth As Integer
        Dim lastday As Integer
        Dim lastyear As Integer

        If today.Month = 1 Then
            lastmonth = 12
            lastday = 31
            lastyear = today.Year() - 1
        Else
            lastmonth = today.Month() - 1
            lastyear = today.Year()

            Select Case lastmonth
                Case 1, 3, 5, 7, 8, 10
                    lastday = 31
                Case 4, 6, 9, 11
                    lastday = 30
                Case 2
                    Select Case today.Year()
                        Case 2016, 2020, 2024, 2028, 2032, 2036, 2040
                            lastday = 29
                        Case Else
                            lastday = 28
                    End Select

            End Select
        End If

        sdate = Convert.ToDateTime(lastmonth & "/1/" & lastyear)
        edate = Convert.ToDateTime(lastmonth & "/" & lastday & "/" & lastyear)


    End Sub

    Private Sub dtpStart_ValueChanged(sender As Object, e As EventArgs) Handles dtpStart.ValueChanged
        sdate = Me.dtpStart.Value
        Me.txtFilename.Text = Me.txtAcct.Text.Replace("@", "") & "_" & sdate.ToString("MMddyyyy") & "-" & edate.ToString("MMddyyyy") & ".txt"
    End Sub

    Private Sub dtpEnd_ValueChanged(sender As Object, e As EventArgs) Handles dtpEnd.ValueChanged
        edate = Me.dtpEnd.Value
        Me.txtFilename.Text = Me.txtAcct.Text.Replace("@", "") & "_" & sdate.ToString("MMddyyyy") & "-" & edate.ToString("MMddyyyy") & ".txt"
    End Sub
End Class
